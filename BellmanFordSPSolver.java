public class BellmanFordSPSolver {
    public static void main(String[] args) {
        Dataset dataset = new Dataset();
        Graph g = dataset.parser(args[0]);

        g.printAdjacencyMatrix();
 
        int r = 0;
        double[] dist = new double[g.V];
        int[] parent = new int[g.V];
        BellmanFord bf = new BellmanFord(g, r, dist, parent);

        if (!bf.hasNegativeCycle()) {
            bf.printParentTable();
            bf.printSPTree(g, r, dist, parent);
            bf.printSP(g, r, dist, parent, dataset.getCities());
        }
    }
}
