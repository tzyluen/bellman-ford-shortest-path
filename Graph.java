public class Graph {
    public int V, E;
    public double adjacencyMatrix[][];
 
    public Graph(int V, int E) {
        this.V = V;
        this.E = E;
        this.adjacencyMatrix = new double[V][V];
        for (int i=0; i<V; ++i) {
            for (int j=0; j<V; ++j)
                this.adjacencyMatrix[i][j] = Double.POSITIVE_INFINITY;
        }
    }

    public void printAdjacencyMatrix() {
        System.out.print("    ");
        for (int i=0; i< this.V; ++i)
            System.out.printf("%6d", i);
        System.out.println();
        System.out.print("    ");
        for (int i=0; i<this.V; ++i)
            System.out.print("------");
        System.out.println();

        for (int i=0; i<this.V; ++i) {
            System.out.printf("%2d:|", i);
            for (int j=0; j<this.V; ++j) {
                if (this.adjacencyMatrix[i][j] == Double.POSITIVE_INFINITY)
                    System.out.printf("%6s", "\u221E");
                else    
                    System.out.printf("%6.2f", this.adjacencyMatrix[i][j]);
            } System.out.println();
        }
    }
}
