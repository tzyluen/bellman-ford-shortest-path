import java.io.*;
import java.util.Scanner;

public class Dataset {
    private Graph G;
    private String[] cities;

    public Graph parser(String filename) {
        System.out.println("Reading: " + filename);
        File file = new File(filename);
        DataInputStream dins = null;
        Scanner scanner = null;
        String line = null;
        int V, E, i = 0;

        try {
            dins = new DataInputStream(new FileInputStream(file));
            scanner = new Scanner(dins);

            while (scanner.hasNextLine()) {
                line = scanner.nextLine();
                if (!(line.charAt(0) == '#'))
                    break;
            }

            V = Integer.parseInt(line.trim());
            this.cities = new String[V];
            String[] city;
            while (scanner.hasNextLine() && i++ < V) {
                city = this.readCity(scanner.nextLine());
                cities[Integer.parseInt(city[0])] = city[1];
            }

            i = 0;
            E = Integer.parseInt(scanner.nextLine().trim());
            this.G = new Graph(V, E);
            while (scanner.hasNextLine() && i < E) {
                line = scanner.nextLine();
                if (!line.equalsIgnoreCase("EOF")) {
                    Edge edge = this.readEdge(line);
                    this.G.adjacencyMatrix[edge.src][edge.dest] = edge.weight;
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            if (scanner!=null)
                scanner.close();
        }

        return this.G;
    }

    public Edge readEdge(String line) {
        String[] s = line.trim().split("\\s+");
        int src  = Integer.parseInt(s[0].trim());
        int dest = Integer.parseInt(s[1].trim());
        double weight = Double.parseDouble(s[2].trim());

        return new Edge(src, dest, weight);
    }

    public String[] readCity(String line) {
        String[] s = line.trim().split("\\s+");
        String i = s[0].trim();
        String name = s[1].trim();

        return new String[]{i, name};
    }

    public String[] getCities() {
        return this.cities;
    }
}
