import java.util.Stack;

public class BellmanFord {
    private Graph graph;
    private int r;
    private double[] dist;
    private int[] parent;
    private boolean nc;

    public BellmanFord(Graph graph, int r, double[] dist, int[] parent) {
        this.graph = graph;
        this.r = r;
        this.dist = dist;
        this.parent = parent;
        this.nc = false;

        // Step 1:
        // Initialize dist[] from r to all other vertices as INFINITY
        // Initialize parent[] of all vertices as INFINITY
        for (int i=0; i<graph.V; ++i) {
            dist[i] = Double.POSITIVE_INFINITY;
            parent[i] = Integer.MAX_VALUE;  // INFINITY constant not available for Integer
        }
        dist[r] = 0.0;  // set r's distance to 0
        parent[r] = -1; // set r's parent to -1
        //printDistTable(this.r);
 
        // Step 2:
        // Relax all edges |V| - 1 times
        // A shortest path from r to any other vertex can have at most |V| - 1 edges
        // A better best case: `changed' flag added to track changes to dist[] occur
        // during a given pass, terminate when no change during a pass
        boolean changed = true;
        for (int pass=1; pass<graph.V && changed; ++pass) {
            System.out.printf("pass %d\n", pass);
            changed = false;
            for (int u=0; u<graph.V; ++u) {
                for (int v=0; v<graph.V; ++v) {
                    if (graph.adjacencyMatrix[u][v] != Double.POSITIVE_INFINITY) {
                        if (dist[u] + graph.adjacencyMatrix[u][v] < dist[v]) {
                            parent[v] = u;
                            System.out.printf("pass(%d): changing dist[%d] from %.2f to",
                                pass, v, dist[v]);
                            dist[v] = dist[u] + graph.adjacencyMatrix[u][v];
                            System.out.printf(" %.2f\n", dist[v]);
                            changed = true;
                        }
                    }
                }
            }
        }
        printDistTable(this.r);
 
        // Step 3:
        // Check for negative-weight cycles
        // Step 2 guarantees shortest distances if the graph doesn't contain -ve weight cycle
        // If we get a shorter path in this pass, there is -ve cycle(s)
        System.out.println("Check for negative cycles:");
        outerloop:
        for (int u=0; u<graph.V; ++u) {
            for (int v=0; v<graph.V; ++v) {
                if (graph.adjacencyMatrix[u][v] != Double.POSITIVE_INFINITY) {
                    if (dist[u] + graph.adjacencyMatrix[u][v] < dist[v]) {
                        System.out.printf("negative cycle detected, dist[%d]:%.2f + " +
                                          "w(%d,%d):%.2f < dist[%d]:%.2f\n", u, dist[u],
                                          u, v, graph.adjacencyMatrix[u][v], v, dist[v]);
                        this.nc = true;
                        break outerloop;  // terminate once detected without further iteration
                    }
                }
            }
        }
        if (!this.hasNegativeCycle())
            System.out.println("No negative cycle");
    }

    public boolean hasPathTo(int v) {
        return this.dist[v] < Double.POSITIVE_INFINITY;
    }

    public Stack pathTo(int v) {
        if (!this.hasPathTo(v))
            return null;

        Stack path = new Stack();
        if (Integer.compare(v, this.r) != 0)
            path.push(v);

        int parent, current = v;
        while (current != this.r) {
            parent = this.parent[current];
            path.push(parent);
            current = parent;
        }

        return path;
    }

    public boolean hasNegativeCycle() {
        return this.nc;
    }

    public void printParentTable() {
        for (int i=0; i<this.graph.V; ++i)
            System.out.printf("%2d parent to %d\n", this.parent[i], i);
    }

    public void printDistTable(int r) {
        for (int i=0; i<this.dist.length; ++i)
            System.out.printf("weight(%d, %d) %.2f\n", r, i, this.dist[i]);
    }

    public void printSPTree(Graph graph, int r, double[] dist, int[] parent) {
        Stack path;
        for (int v=0; v<this.graph.V; ++v) {
            path = this.pathTo(v);
            if (path != null) {
                System.out.printf("%d -> %d (%.2f): ", r, v, dist[v]);
                while (!path.isEmpty()) {
                    int n = (int)path.pop();
                    System.out.printf("%d(%.2f)", n, dist[n]);
                    if (!path.isEmpty())
                        System.out.print("->");
                } System.out.println();
            }
        }
    }

    public void printSP(Graph graph, int r, double[] dist, int[] parent, String[] cities) {
        Stack path;
        for (int v=0; v<this.graph.V; ++v) {
            path = this.pathTo(v);
            if (path != null) {
                System.out.printf("%s -> %s (%.2f): ", cities[r], cities[v], dist[v]);
                while (!path.isEmpty()) {
                    int n = (int)path.pop();
                    System.out.printf("%s(%.2f)", cities[n], dist[n]);
                    if (!path.isEmpty())
                        System.out.print("->");
                } System.out.println();
            }
        }
    }
}
